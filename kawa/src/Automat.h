/*
 * Automat.h
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */


#ifndef AUTOMAT_H_
#define AUTOMAT_H_

#include <iostream>

using namespace std;


class Automat {

friend class Produkt;
public:

	void wybierzProdukt();
	void wydajProdukt();
	void przeliczCene();

	Automat();
	virtual ~Automat();


};

#endif /* AUTOMAT_H_ */
