/*
 * Produkt.h
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
using namespace std;
#include <iostream>

#ifndef PRODUKT_H_
#define PRODUKT_H_

class Produkt {

	//int const ilosc = 8;
public:

	Produkt();
	Produkt(double cena, string nazwa);
	virtual ~Produkt();

private:
	string nazwa;
	double cena;


};

#endif /* PRODUKT_H_ */
