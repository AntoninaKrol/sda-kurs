/*
 * OpenFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef OPENFILEEVENT_H_
#define OPENFILEEVENT_H_

#include "Event.h"

class OpenFileEvent: public Event {
public:
	OpenFileEvent();
	virtual ~OpenFileEvent();
};

#endif /* OPENFILEEVENT_H_ */
