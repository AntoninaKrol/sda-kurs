/*
 * TestClass.h
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef TESTCLASS_H_
#define TESTCLASS_H_

class TestClass {
public:
	TestClass();
	virtual ~TestClass();
	int add_number(int value, int number);

private:
	int fib[20];
};

#endif /* TESTCLASS_H_ */
