#include "gtest/gtest.h"
#include "TestClass.h"

TEST(TestingPowers, One) {
	EXPECT_EQ(1, secondPower(1));
}

TEST(TestingPowers, Five) {
	EXPECT_EQ(25, secondPower(5));
}
TEST (TestingPowers, Negative) {
	EXPECT_EQ(1, secondPower(-1));
}
TEST (TestingPowers, Border) {
	EXPECT_EQ(100000000, secondPower(10000));
}

