//============================================================================
// Name        : cpp.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<typeinfo>
using namespace std;

class Foo {
public:
	virtual~Foo(){}
};

class Bar{
public:
	virtual~Bar(){}
};

void displayException(exception& e) {
	cout << "exception: " << e.what() << endl;
}

void badException() throw (bad_exception){
		throw runtime_error("moj wyjatek");//bad exception
}

int main() {
	Bar b;
	try {
		//int *test = new int[1000000];
		//Foo &f=dynamic_cast<Foo&>(b);
		badException();


	} catch (bad_alloc&e) {
		displayException(e);

	} catch (bad_cast&e) {
		displayException(e);

	} catch (bad_exception&e) {
		displayException(e);

	} catch (bad_typeid&e) {
		displayException(e);

	} catch (ios_base::failure&e) {
		displayException(e);

	} catch (...) {

		cout << "unrecognized exception" << endl;
	}

	return 0;

}
