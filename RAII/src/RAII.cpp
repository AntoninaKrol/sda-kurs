//============================================================================
// Name        : RAII.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include "LogWriter.h"
using namespace std;



int main() {
//	fstream f("logi.log", ios_base::out);
//	f<<"Hello";
//	f.close();
	LogWriter a;
	a.logMessage("string", LogWriter::Trace);
	a.logMessage("string", LogWriter::Error);
	a.logMessage("string", LogWriter::Info);
	a.logMessage("string", LogWriter::Debug);


	return 0;
}
