/*
 * LogWriter.h
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <fstream>
using namespace std;

class LogWriter
{
public:
	LogWriter();
	enum LogType {Fatal, Error, Warning, Info, Debug, Trace};
	void setLoggingLevel(LogType logLevel);
	void logMessage (string message, LogType messageType);
	virtual ~LogWriter();
private:
	fstream first;
	LogType logLevel;
};

#endif /* LOGWRITER_H_ */
