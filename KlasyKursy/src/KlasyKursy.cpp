#include <iostream>
#include <string>
#include "Room.h"
#include "Osoba.h"

using namespace std;

int main()

{
    std::cout<<"------Kursy-------"<<std::endl;
    Room tab_Room [10];
    tab_Room [0]=Room(1,30,1);
    tab_Room [1]=Room(2,30,2);
    tab_Room [2]=Room(3,30,3);
    tab_Room [3]=Room(4,35,4);
    //r1.dodajKurs("Matematyka");
    //r1.dodajKurs("Informatyka");
    //r1.dodajKurs("Fizyka");
    //r1.opis();
    std::cout<<"---------------------"<<std::endl;

    std::cout<<"------Studenci-------"<<std::endl;

    Osoba tab_osob [10];
    tab_osob[0]=Osoba("Student","Karol","Zdeb","zdeb@gmail.com", 24);
    tab_osob[1]=Osoba("Student", "Ela", "Fula","ela@gmail", 34);
    tab_osob[2]=Osoba("Student","Wojciech", "Cyran", "wcyran@gmail.com", 29);
        for (int i=0; i<3; i++)
        {
            tab_osob[i].opis();
        }

    std::cout<<"------Profesorzy-------"<<std::endl;

    tab_osob[3]=Osoba("Profesor","Jerzy","Grebosz","jerzy@gmail.com", 65);
    tab_osob[4]=Osoba("Profesor","Zbigniew","Pieczonka","pieczony@gmail.com", 52);
    tab_osob[5]=Osoba("Profesor","Stanislaw","Czech","czech@gmail.com", 68);

        for (int i=0; i<3; i++)
        {
            tab_osob[i].opis();
        }


    //r1.getNRoom();
    //r1.setNRoom(2)

    return 0;
}
