#ifndef ROOM_H
#define ROOM_H
#include <iostream>
#include <string>



class Room
{
    public:
        Room();
        Room(int nRoom, int pojRoom, int Building); // konstruktor musi byc taki sam w .h i .cpp
        void opis();
        void dodajKurs (std::string kurs); // metoda zliczajaca kursu w sali przy uzyciu tablicy - powstaje zmienna nazwa kursu za numer kursu jest zliczany
        void kursyWSali ();
        int getNRoom ();              //uzywane tylko w main        // get zwraca nr pokoju
        void setNRoom (int nRoom) {this ->nRoom=nRoom;}     // set wyswietla i przyjmuje atrybut wartosci nr pokoju // set przestawia zmienna
        // ustawia wartoscie w konstruktorze,
    protected:

    private:
        int nRoom;
        int pojRoom;
        int numKursuWSali;
        int Building;
        std::string lista_kursow[20]; // tablica listy kursu do metody zliczajacej i przypisujacej nazwe i nr kursu
};

#endif // ROOM_H
