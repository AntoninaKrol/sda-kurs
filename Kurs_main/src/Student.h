/*
 * Student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_
#include<iostream>
using namespace std;

class Student {
public:
	Student();
	string imie;
	string nazwisko;
	string kursStudenta;
	string email;
	int wiek;
	void opisStudenta();
	void kursyStudenta();
	void zapiszNaKurs();
};

#endif /* STUDENT_H_ */
