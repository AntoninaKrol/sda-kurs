//============================================================================
// Name        : Kurs_main.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Sala.h"
#include "Kurs.h"
using namespace std;

int main() {
	int i;
	Sala* sala[3];
	Sala angielski;
	Sala francuski;
	Sala hiszpanski;
	sala[0]=&angielski;
	sala[1]=&francuski;
	sala[2]=&hiszpanski;
	angielski.numer=1;
	angielski.kurs="angielski";
	angielski.pojemnoscSali=100;
	francuski.numer=2;
	francuski.kurs="francuski";
	francuski.pojemnoscSali=70;
	hiszpanski.numer=3;
	hiszpanski.kurs="hiszpanski";
	hiszpanski.pojemnoscSali=40;
//	angielski.dodajKurs();
//	angielski.jakiKurs();
//	angielski.opisSali();
//	francuski.dodajKurs();
//	francuski.jakiKurs();
//	francuski.opisSali();
//	hiszpanski.dodajKurs();
//	hiszpanski.jakiKurs();
//	hiszpanski.opisSali();
	for(int i=0; i<3; ++i){
	sala[i]->dodajKurs();
	sala[i]->opisSali();
	sala[i]->jakiKurs();

	}

	return 0;
}
