//============================================================================
// Name        : wskazniki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int*wi;
	double*wd;
	int tabint[10]={0,1,2,3,4,5,6,7,8,9};
	double tabdbl[10];

	wd=tabdbl;
	for(int i=0; i<10; ++i)
	{
		*(wd++)=i/10.0;
	}
	cout<<"tresc tablicy na poczatku"<<endl;

	wi=tabint;
	wd=tabdbl;
	for(int k=0; k<10; ++k)
	{
		cout<<k<<" / "<<*wi<<" / "<<*wd<<endl;
		wi++;
		wd++;
	}

	//wi=&tabint[4];
	wi=tabint+4;
	wd=tabdbl+2;

	for(int p=0; p<4; ++p)
	{
		*(wi++)=90;
		*(wd++)=0.25;
	}

	wi=tabint;
	wd=tabdbl;

	for(int z=0; z<10; ++z)
	{
		cout<<z<<" / "<<*wi++<<" / "<<*wd++<<endl;
	}

	return 0;
}
