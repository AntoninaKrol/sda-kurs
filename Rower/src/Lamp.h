/*
 * Lamp.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#ifndef LAMP_H_
#define LAMP_H_

class Lamp {
public:
	Lamp();
	enum LightsConfiguration{przod, tyl, oba};
private:
	string nazwa;
	bool front;
	bool back;
	int odblask;

public:
	void wlacz(LightsConfiguration configuration);
	void wylacz(LightsConfiguration configuration);
	void sprawdz();
};

#endif /* LAMP_H_ */
