/*
 * Bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_
#include "Frame.h"
#include "Wheels.h"
#include "Lamp.h"
#include "DriveSystem.h"

class Bike {
public:
	Bike();
	int frontWheelSize()const;

	void check();

private:
	Frame frame;
	Wheels frontWheel;
	Wheels backWheel;
	Lamp lights;
	DriveSystem driveSystem;
};

#endif /* BIKE_H_ */



/*rower posiada:
	ko�a x2 							klasa
		typ opony

	rama								klasa
		sztywna
		amortyzowana
	hamulec x2
	amortyzator
		rodzaj
	kierownica
	siode�ko
	lampka(prz�d, ty�, odblaski)	klasa
	metody:
		w��cz
		wy��cz
	uk�ad napedowy:						klasa
		�a�cuch
		biegi prz�d
		biegi ty�
		przerzutka
		peda�a
	metody:
		zmie� bieg

	metody:
		jed�
		hamuj
		w��cz swiat�a
		skr�c (kierunek)
		zmien bieg	jedna metoda z dwoma argumentami (przerzutka prz�d/ty�, kierunek g�ra/d�),
					albo cztery metody bez argument�w*/
