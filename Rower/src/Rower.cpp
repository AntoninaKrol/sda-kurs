//============================================================================
// Name        : Rower.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Lamp.h"
#include "Frame.h"
#include "Wheels.h"
#include "DriveSystem.h"
#include "Bike.h"
using namespace std;

int main() {
	Lamp swiatlo;
	swiatlo.wlacz(Lamp::przod);
	swiatlo.wylacz(Lamp::przod);
	Frame rama (Frame::other);
	Wheels kolo;
	kolo.wyswietl();
	DriveSystem przerzutka;
	przerzutka.show();
	swiatlo.sprawdz();
	Bike rower;
	rower.check();
		return 0;
}
