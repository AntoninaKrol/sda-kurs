/*
 * DriveSystem.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef DRIVESYSTEM_H_
#define DRIVESYSTEM_H_
using namespace std;

class DriveSystem {
public:
	DriveSystem(int front_Shift=6, int back_Shift=3);
	void show ();
private:
	int frontShift;
	int backShift;
	int maxFrontShift;
	int maxBackShift;
};

#endif /* DRIVESYSTEM_H_ */
