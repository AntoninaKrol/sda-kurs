/*
 * Frame.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef FRAME_H_
#define FRAME_H_

class Frame {
public:
	enum frameType{hardtail, other};
	enum frameSize{S, M, L, XL};
	Frame(frameType fType);
	Frame(frameSize sType);
private:
	frameType type;
	frameSize size;
};

#endif /* FRAME_H_ */
