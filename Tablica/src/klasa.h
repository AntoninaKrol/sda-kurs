/*
 * klasa.h
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef KLASA_H_
#define KLASA_H_
const int tab_size=1000;
class klasa {
public:
	klasa();
	virtual ~klasa();
	void add_element(double element);
	void add_the_last_element (double element);
	void substract_element();
	double show_element(int number_of_elements);
	int return_size();
	double* return_wsk();
	double max_element();
	double min_element();
	double sum_element();
	void removeElementFromIndexReduceArraySize(int index);
	double addElementToIndexIncreaseArraySize(int index, double value);



private:
	double tab[tab_size];
	int number_of_elements;
};

#endif /* KLASA_H_ */
