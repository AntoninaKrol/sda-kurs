#include "klasa.h"
#include  "../gtest/gtest.h"


TEST(TestingSupertab, NewTabZeroPlace123) {
	klasa zero;
	EXPECT_EQ(1, zero.return_size() );}

TEST(TestingSupertab, AddMoreElements) {
	klasa zero;
	EXPECT_EQ(1, zero.return_size() );
	zero.add_element(12.5);
	zero.add_element(5.3);
	zero.add_element(13.8);
	zero.add_element(12.1);
	zero.add_element(58.5);
	EXPECT_EQ(6, zero.return_size() );
}

TEST(TestingSupertab, ShowElement) {
	klasa zero;
	zero.add_element(12.5);
	zero.add_element(5.3);
	zero.add_element(13.8);
	zero.add_element(12.1);
	zero.add_element(58.5);

	EXPECT_EQ(58.5, zero.show_element(5));}

TEST(TestingSupertab, AddLastElement) {
	klasa last;
	last.add_element(58.5);

	EXPECT_EQ(58.5, last.show_element(1));
	//EXPECT_EQ(2, last.return_size());
	}

TEST(TestingSuperTab, SubstractElement){
	klasa last;
	last.add_element(14.8);
	last.add_element(14.9);
	last.add_element(19.8);
	last.substract_element();
	//EXPECT_EQ(2, last.return_size());
	EXPECT_EQ(123, last.show_element(0));
	EXPECT_EQ(14.8, last.show_element(1));
	EXPECT_EQ(14.9, last.show_element(2));
}

TEST(TestingSupertab, ShowAdressFirstElement) {
	klasa last;
	last.return_wsk();
	//123==*(last.return_wsk());

	EXPECT_EQ(123, *(last.return_wsk()));}

TEST(TestingSuperTab, ShowMaxi){
	klasa last;
	last.add_element(148);
	last.add_element(149);
	last.add_element(198);
	EXPECT_EQ(198, last.max_element());}

TEST(TestingSuperTab, ShowMini){
	klasa last;
	last.add_element(148);
	last.add_element(149);
	last.add_element(198);
	EXPECT_EQ(123, last.min_element());}

TEST(TestingSuperTab, SumElement){
	klasa last;
	last.add_element(1);
	last.add_element(2);
	last.add_element(3);
	EXPECT_EQ(129, last.sum_element());}

TEST(TestingSuperTab, AddAnyElement){
	klasa last;
	last.add_element(1);
	last.add_element(2);
	last.add_element(3);
	last.addElementToIndexIncreaseArraySize(2,2.5);
	EXPECT_EQ(5, last.return_size());
	EXPECT_EQ(2.5, last.show_element(2));}

TEST(TestingSuperTab, RemoveAnyElement){
	klasa last;
	last.add_element(1);
	last.add_element(2);
	last.add_element(3);
	last.removeElementFromIndexReduceArraySize(2);
	EXPECT_EQ(3, last.return_size());
	EXPECT_EQ(3, last.show_element(2));}
