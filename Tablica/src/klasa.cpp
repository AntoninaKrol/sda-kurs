/*
 * klasa.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "klasa.h"
#include<iostream>
using namespace std;

klasa::klasa() :
		number_of_elements(0) {
	add_element(123);
}

void klasa::add_element(double element) {
	tab[number_of_elements] = element;
	++number_of_elements;
}

int klasa::return_size() {
	return number_of_elements;
}

double klasa::show_element(int number_of_elements) {

	return tab[number_of_elements];
}

void klasa::substract_element() {
	if (number_of_elements > 0) {
		--number_of_elements;
	};
}

double* klasa::return_wsk() {
	double *wsk;
	wsk = tab;
	return wsk;
}

double klasa::max_element() {
	double mini = 1000;
	double maxi = 0;
	for (int i = 0; i < number_of_elements; ++i) {

		if (maxi < tab[i])
			maxi = tab[i];

	}
	return maxi;
}

double klasa::min_element() {
	double mini = 1000;
	double maxi = 0;
	for (int i = 0; i < number_of_elements; ++i) {

		if (mini > tab[i])
			mini = tab[i];
	}
	return mini;
}

double klasa::sum_element(){
	double sum=0;
	for (int i=0; i<number_of_elements; ++i){
		sum=sum+tab[i];
	}
	return sum;
}

void klasa::removeElementFromIndexReduceArraySize(int index){
    for (int i=index;i<number_of_elements;i++){
        tab[i]=tab[i+1];
    }
    number_of_elements=number_of_elements-1;
   }

double klasa::addElementToIndexIncreaseArraySize(int index, double value){
	number_of_elements=number_of_elements+1;
    for (int i=number_of_elements;i>=index;--i){
        tab[i+1]=tab[i];
    }
    tab[index]=value;
    return tab[index];
}

klasa::~klasa() {

}

