//============================================================================
// Name        : Projekt.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <cmath>
#include <iostream>
using namespace std;

int main()
{
  int n;
  int potega = 1;

  cout<<"Podaj ilo�� kolejnych pot�g liczby 2 do wy�wietlenia: ";
  cin>>n;

  for(int i = 0;i<n;i++, potega<<=1)
    cout<<"2 do pot�gi "<<i<<" wynosi "<<potega<<endl;

  return 0;
}
