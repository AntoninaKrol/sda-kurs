/*
 * Room.h
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#ifndef ROOM_H_
#define ROOM_H_
#include <iostream>
using namespace std;

class Room {

private:
	int number;
	int capacity;
	string building;
	int numKursuWSali;
	string lista_kursow[20]

public:
	Room();
	Room(int number, int capacity, string building);
	void description();
	void add_course(string course);
	void course_room();

};

#endif /* ROOM_H_ */



