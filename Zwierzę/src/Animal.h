/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ANIMAL_H_
#define ANIMAL_H_
#include <string>

class Animal {
public:
	Animal(std::string givenName);
	void speak();
	std::string giveAName(){return std::string("my name is: ") + name;}
private:
	virtual void giveASound()=0;//klasa abstrakcyjna bo ta jedna metoda wirtualna
	std::string name;
};

#endif /* ANIMAL_H_ */
