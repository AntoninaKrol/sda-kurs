//============================================================================
// Name        : Zwierz�.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Animal.h"
#include "Cat.h"
#include "Dog.h"
#include "Bee.h"
#include "Insects.h"
using namespace std;

int main() {
	const int numberOfAnimals=4;
	Animal* animals[numberOfAnimals];
	//Animal anAnimal("thing");
	Animal *someAnimal;
	Dog reks("reks");
	Dog fafik("fafik");
	Cat bruno("bruno");
	Cat pluto("pluto");
	Cat mruczek("mruczek");
	Bee maja("maja");
	//anAnimal.speak();
	reks.speak();
	mruczek.speak();
	animals[0]=&fafik;
	animals[1]=&bruno;
	animals[2]=&reks;
	animals[3]=&maja;
	std::cout<<reks.giveAName()<<std::endl;
	std::cout<<fafik.giveAName()<<std::endl;
	//someAnimal=&reks;
	//someAnimal->speak();
	//someAnimal=&mruczek;
	//someAnimal->speak();
	for(int i=0; i<numberOfAnimals; ++i)
	{
		animals[i]->speak();

	//}
	//using Class Base{
	//	void f(int);
	};
	return 0;
}
