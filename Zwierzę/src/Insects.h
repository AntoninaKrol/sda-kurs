/*
 * Insects.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef INSECTS_H_
#define INSECTS_H_

#include "Animal.h"

class Insects: public Animal {

	public:
	Insects(std:: string givenName);
	private:
	void giveASound();

};

#endif /* INSECTS_H_ */
