/*
 * Dog.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DOG_H_
#define DOG_H_

#include "Animal.h"

class Dog: public Animal {
public:
	Dog(std:: string givenName);
	private:
	void giveASound();
};

#endif /* DOG_H_ */
