/*
 * A.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef A_H_
#define A_H_

class B;
#include <iostream>
class A {
public:
	friend std::ostream& operator<<(std::ostream& stream, const A& a);
	friend B;
	A();
	virtual ~A();
	int number;
private:
	int n;
};
std::ostream& operator<<(std::ostream& stream, const A& a);
#endif /* A_H_ */
