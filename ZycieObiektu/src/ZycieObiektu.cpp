//============================================================================
// Name        : ZycieObiektu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"
#include "D.h"
using namespace std;

//A globalA;

void f(A& someA)
{
cout<<someA.number<<endl;
someA.number=5;
//globalA.number=54;
cout<<someA.number<<endl;
}

int main() {
	cout<<"Hello World"<<endl;
	//A tabA[10];
//	A a;
//	a.number=2;
//	cout<<"global a: "<<globalA.number<<endl;
//	f(a);
//	cout<<"after calling f(a): "<<a.number<<endl;
//	cout<<"global a: "<<globalA.number<<endl;
//	cout<<a;
	B b;
//	B bc;
//	bc=b;
	cout<<"Pause"<<endl;
	B bb(b);
	cout<<"Something more complicated: "<<endl;
//	A *a = new D;
//	delete a;
//	cout <<"!!!Exciting the program!!!"<<endl;
	return 0;
}
