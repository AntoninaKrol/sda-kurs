#include "gtest/gtest.h"
#include "functions.h"

TEST(TestingPowers, One) {
	EXPECT_EQ(1, secondPower(1));
}

TEST(TestingPowers, Five) {
	EXPECT_EQ(25, secondPower(5));
}
TEST (TestingPowers, Negative) {
	EXPECT_EQ(1, secondPower(-1));
}
TEST (TestingPowers, Border) {
	EXPECT_EQ(100000000, secondPower(10000));
}

TEST(TestingFactorial, Two) {
	EXPECT_EQ(2, factorial(2));
}

TEST(TestingFactorial, Three) {
	EXPECT_EQ(6, factorial(3));
}
TEST (TestingFactorial, Negative) {
	EXPECT_EQ(0, factorial(-3));
}
TEST (TestingFactorial, Border) {
	EXPECT_EQ(3628800, factorial(10));
}

TEST(TestingFibonacci, Two) {
	EXPECT_EQ(2, fibonacci(2));
}

TEST(TestingFibonacci, Thirteen) {
	EXPECT_EQ(377, fibonacci(13));
}
TEST (TestingFibonacci, Negative) {
	EXPECT_EQ(0, fibonacci(-3));
}
TEST (TestingFibonacci, Border) {
	EXPECT_EQ(7778742049, fibonacci(50));
}
TEST (TestingFibonacci, Less) {
	EXPECT_LT(7, fibonacci(8));
}
TEST (TestingFibonacci, Greater){
	EXPECT_GT(14, fibonacci(6));
}
