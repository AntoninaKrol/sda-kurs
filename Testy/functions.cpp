#include <cmath>
#include <iostream>
using namespace std;

int secondPower(int number) {
	return (number * number);
}

int factorial(int number) {
	int silnia = 1;
	if (number < 0) {
		return false;
	} else if (number == 0) {
		return 1;
	} else if (number > 0) {
		for (int i = 1; i <= number; ++i) {
			silnia = i * silnia;
		}
	}
	return silnia;
}

int fibonacci(int number) {

	if (number <= 0) {
		return 0;
	} else if (number == 1) {
		return 1;
	} else {
		int fib[number];
		fib[0] = 1;
		fib[1] = 1;
		for (int i = 2; i <= number; i++) {
			fib[i] = fib[i - 1] + fib[i - 2];
		}
		return fib[number];
	}
	return 0;
}

int roundNumbers(int number) {
	if (number < 0 && number > 100) {
		return -1;
	} else if (number > 40 && number <= 100) {
		if (number%5==3||number%5==4) {
			number = number + 5 - (number % 5);
			return number;
		}
	} else {
		return number;
	} return -2;
}
