/*
 * Triangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include<iostream>
#include "Figures.h"
using namespace std;

class Triangle: public Figures {
public:
	Triangle(string givenName);
	double givenArea();
	double a;
	double h;
	void draw();
};

#endif


