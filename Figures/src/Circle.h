/*
 * Circle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
#include<iostream>
using namespace std;
#include "Figures.h"

class Circle: public Figures {
public:
	Circle(string givenName);
	double givenArea();
	double r;
	void draw();
};

#endif

