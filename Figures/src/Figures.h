
#ifndef FIGURES_H_
#define FIGURES_H_
#include <string>
using namespace std;

class Figures {
public:
	Figures(string givenName);
	void givenName();
	virtual double givenArea()=0;
	virtual void draw()=0;
	string name;
};

#endif /* FIGURES_H_ */



