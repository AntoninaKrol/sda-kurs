/*
 * Rectangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include "Square.h"
#include <iostream>
using namespace std;

class Rectangle: public Square {
public:
	Rectangle(string givenName);
	double givenArea ();
	double a;
	double b;
	void draw ();
};

#endif
