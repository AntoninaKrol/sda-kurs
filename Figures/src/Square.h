#ifndef SQUARE_H_
#define SQUARE_H_
#include "Figures.h"
#include<iostream>
using namespace std;

class Square: public Figures {
public:
	Square(string givenName);
	double givenArea();
	double a;
	void draw();
};

#endif
