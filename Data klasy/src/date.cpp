/*
 * date.cpp
 *
 *  Created on: 22.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "date.h"
using namespace std;

int Date::getDay() const{
	return day;
}
void Date::setDay(int nday) {
	day=nday;
}
int Date::getMonth(){
	return month;
}
void Date::setMonth(int nmonth) {
	month=nmonth;
}
int Date::getYear(){
	return year;
}
void Date::setYear(int nyear) {
	year=nyear;
}
bool Date::isValid() const{

		if (month<1||month>12)
		return false;

		switch (month){
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			{if(day<1||day>31)
				return false;}
		break;
		case 4:
		case 6:
		case 9:
		case 11:
			{if(day<1||day>30)
				return false;}
		break;
		case 2:
			{if (((year%4==0 && year%100!=0) || year%400==0)&&(day>=1||day<=29))
				return true;
			else if (day<1||day>28)
				return false;
			else
				return false;}
			break;
		}

	return true;
}

void Date::printDate() const{
	cout<<day<<"/"<<month<<"/"<<year<<endl;
}
void Date::addDay(){
	day=day+1;
}
void Date::addMonth(){
	month=month+1;
	int a = month/12;
	int b = month%12;
	month=month+b;
	year=year+a;
}
void Date::addYear(){
	year=year+1;
}
void Date::addDays(int ndays){
		day=day+ndays;
	}
void Date::addMonths(int nmonths){
		month=month+nmonths;
		int a = month/12;
		int b = month%12;
		if (b==0){
		month=12;}
		else
		month=b;
		year=year+a;
	}
void Date::addYears(int nyears){
	year=year+nyears;
	}
