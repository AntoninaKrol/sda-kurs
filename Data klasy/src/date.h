/*
 * date.h
 *
 *  Created on: 22.03.2017
 *      Author: RENT
 */

#ifndef DATE_H_
#define DATE_H_

class Date{
public:
	Date();
	Date(int nday, int nmonth, int nyear=2017); //konstruktor
	int getDay()const;
	void setDay(int day);
	int getMonth();
	void setMonth(int month);
	int getYear();
	void setYear(int year);
	bool isValid()const;
	void printDate()const;
	void addDay();
	void addMonth();
	void addYear();
	void addDays(int days);
	void addMonths(int months);
	void addYears(int years);
	/*bool operator==(const Date& other)const{
		if (day==other.day&&month==other.month&&year==other.year)
			return true;
		return false;
	}
	bool operator!=(const Date& other)const{
		return !operator==(other);
		return !(*this==other);
	}*/

	bool operator<(const Date& other)const{
		if (year<other.year){
			return true;
		}
		else if(year==other.year){
			if(month<other.month){
				return true;
			}
			else if(month==other.month){
				if(day<other.day){
					return true;
				}}

		}return false;
		}


private:
	int day;
	int month;
	int year;
};



#endif /* DATE_H_ */
