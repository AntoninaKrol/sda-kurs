#include<iostream>
using namespace std;

class Prostokat;

class Punkt{
public:
	Punkt(string="A", float=0, float=0);
	void wczytaj();
	friend void sedzia(Punkt pkt, Prostokat p);
private:
	string name;
	float x;
	float y;
};

class Prostokat{
public:
	Prostokat(string="Brak",float=0, float=0, float=1, float=1);
	void wczytaj();
	friend void sedzia(Punkt pkt, Prostokat p);
private:
	string name;
	float x;
	float y;
	float wys;
	float szer;
};
