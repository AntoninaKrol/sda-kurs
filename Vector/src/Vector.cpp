//============================================================================
// Name        : Vector.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<vector>
using namespace std;

int main() {

	vector<int> myvector;
	for (int i = 0; i < 10; i++)
		myvector.push_back(i);

	cout << "myvector contains:";
	for (vector<int>::iterator it = myvector.begin(); it != myvector.end();
			++it)
		cout << ' ' << *it;
	cout << '\n';
//	for (vector<int>::iterator it = myvector.begin() + 7; it != myvector.end();
//			++it)
//		*it = (*it) * (*it);
//	for (vector<int>::iterator it = myvector.begin(); it != myvector.end();
//			++it)
//		cout << ' ' << *it;
//	cout << '\n';

	vector<int> mynewvector;
	for (int i = 48; i < 54; i++)
		myvector.push_back(i);

	cout << "mynewvector contains:";
	for (vector<int>::iterator itn = mynewvector.begin();
			itn != mynewvector.end(); ++itn)
		cout << ' ' << *itn;
	cout << '\n';

	return 0;
}
